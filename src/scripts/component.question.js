events.on('ready', function () {
    var countAlternativesCorrect = 0;

    $(".container-template-11").find('.feedback .modal-close').attr('onclick', 'navigate.next()');

    $(".container-template-11").find('.container-question .wrapper-altervatives .container-alternative').each(function() {
        if( $(this).data('status-alternative') ) {
            countAlternativesCorrect++;
        }
    });

    $(".container-template-11").find('.container-question .wrapper-altervatives .container-alternative').click(function() {
        $(this).toggleClass('actived');
    });

    $(".container-template-11").find('.container-question .button-confirm').click(function() {
        var countQuestionResponse = 0;

        $(".container-template-11").find('.container-question .wrapper-altervatives .container-alternative.actived').each(function() {
            if( $(this).data('status-alternative') ) {
                countQuestionResponse++;
            }else {
                countQuestionResponse--;
            }
        });

        console.log(countQuestionResponse, countAlternativesCorrect);

        if(countQuestionResponse === countAlternativesCorrect) {
            $(".container-template-11").find('#feedback-positive').removeClass('hide');
        }else {
            $(".container-template-11").find('#feedback-negative').removeClass('hide');
        }
    });
});