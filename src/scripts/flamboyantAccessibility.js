var acessOptions = {
    labels:{
        menuTitle: ' ',
        increaseText: 'Aumentar',
        decreaseText: 'Diminuir',
        increaseTextSpacing: 'Aumentar espaçamento das fontes',
        decreaseTextSpacing: 'Diminuir espaçamento das fontes',
        invertColors: 'Inverter cores',
        grayHues: 'Tonalidade cinza',
        underlineLinks: 'Sublinhar links',
        bigCursor: 'Aumentar o cursor',
        readingGuide: 'reading guide (in my language)',
        textToSpeech: 'text to speech (in my language)',
        speechToText: 'speech to text (in my language)'
    },
    textToSpeechLang: 'pt-BR',
    speechToTextLang: 'pt-BR',
    // textPixelMode: true,
	hotkeys:{
		enabled: true
    },
    
	icon: {
		circular: true,
        img: 'accessibility_new',
        position: {
            top: { size: 0, units: 'px' },
            left: { size: 0, units: 'px' },
            type: 'fixed'
        }
    },
    modules: {
        increaseText: true,
        decreaseText: true,
        invertColors: true,
        increaseTextSpacing: true,
        decreaseTextSpacing: true,
        grayHues: true,
        underlineLinks: true,
        bigCursor: true,
        readingGuide: false,
        textToSpeech: false,
        speechToText: false
    },
    textPixelMode: true
}


events.on('ready', function () {
    setTimeout(function () {
        setAcess();
        $(getFirstTabElement()).focus();
    }, 500);


    //Iniciando as ferramentas de acessibilidade apenas se não for IE.
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
    {
        //Caso seja IE
    }else{
        new Accessibility(acessOptions);
    }

});

function getFirstTabElement(){
    var element = null;
    $("*").each(function () {
        if($(this).attr("tabindex") == 1 || $(this).attr("tabindex") == "1"){
            element = $(this);
        }
        
    });

    return element;
}

function setAcess() {
    var elements = '*';

    $("*").each(function () {
        if ($(this).attr("alt") != undefined && $(this).attr("aria-label") == undefined) {
            $(this).attr("aria-label", $(this).attr("alt"));
        }

        // if ($(this).attr("tabindex")) {
        //     if(!$(this).attr("aria-label")){
        //         $(this).attr("aria-label", "Teste");
        //     }
        // }

        if ($(this).attr("onclick")) {
            
            $(this).keydown(function(event){
                if (event.key === " " || event.key === "Enter" || event.key === "Spacebar") {
                    $(this).click();
                    event.preventDefault();
                }
            });

            $(this).attr("role", "button");
        }


        // onKeyDown
    });


    
    $(".modal").each(function () {
        var _tabndex = findLastIndex($(this));
        $(this).find('.modal-close').attr("tabindex", Math.round(_tabndex + 1));
        $(this).find('.modal-close').attr("aria-label", "Fechar modal.");
    });


    // $(".slide").each(function () {
    //     var _tabndex = findLastIndex($(this));
    //     $(this).find('.modal-close').attr("tabindex", Math.round(_tabndex + 1));
    // });
    
    var arrSlides = $(".slide");
    var lastSlide = arrSlides[arrSlides.length-1];
    $(lastSlide).focusin(function() {
        $(".container-arrow-right").css("display", "block");
    });
    // console.log(lastSlide);

 

    bridge.hiddeViewsFromReaderScreen();
}



function findLastIndex(element){
    var tabMax = 0;
    $(element).find("*").each(function () {
        var tabAtual = Number($(this).attr('tabindex'));
        
        if(tabAtual!=undefined){
            if(tabAtual > tabMax){
                tabMax = tabAtual;
            }
        }
        
    });

    return tabMax;
}