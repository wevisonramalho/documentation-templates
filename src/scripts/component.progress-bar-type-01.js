events.on('ready', function () {
    const getBarCorrect = scorm.loadObject('bar-correct');
    const getBarError = scorm.loadObject('bar-error');

    setTextBar(getBarCorrect,getBarError);
    setProgress();
});

function setProgress() {
    $(".container-progress-bar").find('.bar').each(function() {
        var countPages = navigate.currentScreen.model.pages.length;
        var actualPage = Math.round(navigate.currentScreen.index + 1);
        var progress = Math.round(100 / countPages * actualPage);

        $(this).find('.progress').css('width', (progress) + "%");
        $(this).parents('.container-progress-bar').find('.content-text-before .text').text(`${ progress }%`);
    });
}

function setTextBar(getBarCorrect,getBarError) {
    var valueBarCorrect = getBarCorrect,
        valueBarError = getBarError;

    if(valueBarCorrect < 0) {
        valueBarCorrect = 0;
    }

    if(valueBarError > 100) {
        valueBarError = 100;
    }

    $(".bar-01").find('.text-progress').text(Math.round(valueBarCorrect) < 1 ? '' : Math.round(valueBarCorrect));
    $(".bar-02").find('.text-progress').text(Math.round(valueBarError) < 1 ? '' : Math.round(valueBarError));
}