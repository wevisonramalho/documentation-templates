events.on('ready', function () {
    $(".menu-outside").click(function() {
        $("body").addClass('blocked');
        $(this).addClass('hide');
        $(".content-menu").removeClass('hide');
    });

    $(".menu-inside").click(function() {
        $("body").removeClass('blocked');
        $(".content-menu").addClass('hide');
        $(".menu-outside").removeClass('hide');
    });
});