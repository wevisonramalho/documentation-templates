events.on('ready', function () {
    var valueStatus = 0;

    $(".container-quiz").each(function() {
        $(this).find(".label").focus(function() {
            var $element = $( this );

            document.body.onkeyup = function(e){
                if(e.keyCode == 13){
                    $element.find('.input-radio').click();
                }
            }
        });
        
        $( this ).find('.input-radio').click(function() {
            valueStatus = controllAlternative( $(this), valueStatus );
            controllButtonConfirm( $(this), valueStatus )
        });
    });
});

function controllAlternative($element, valueStatus) {
    valueStatus = !!parseInt($element.data('status'));

    $element.parents('.container-quiz').find('.container-check .label').removeClass('actived');
    $element.parents('label').addClass('actived');

    $element.parents('.container-quiz').find('.footer').removeClass('hide');
    $element.parents('.container-quiz').find('.input-radio').prop('checked', false);
    $element.prop('checked', true);

    $element.parents('.wrapper-inputs').find('.input-radio').attr('aria-selected', 'false');
    $element.attr('aria-selected', 'true');

    return valueStatus;
}

function controllButtonConfirm($element, valueStatus) {
    $element.parents('.container-quiz').find('.button-confirm').click(function() {
        $element.parents('.container-quiz').find('.wrapper-alternatives').hide();
        $element.parents('.container-quiz').find('.footer').hide();

        if( valueStatus ) {
            $element.parents('.container-quiz').find('.wrapper-feedback .feedback-positive').removeClass('hide');
            $element.parents('.container-quiz').find('.wrapper-feedback .feedback-negative').attr('tabindex', '-1');
        } else {
            $element.parents('.container-quiz').find('.wrapper-feedback .feedback-negative').removeClass('hide');
            $element.parents('.container-quiz').find('.wrapper-feedback .feedback-positive').attr('tabindex', '-1');
        }

        return $element.parents('.container-quiz').find('.wrapper-feedback .feedback .image').focus();
    });
}