events.on('ready',function(){
    var jsonURL = "../../data/languages.json";

    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: jsonURL,
        success: function success(arrayLangagues) {
            $('body').find('.translate').each(function(index) {
                var getlanguage = scorm.loadObject('language'),
                    getParameterLanguage = $(this).data('language');
        
                $(this).html( arrayLangagues[0][getlanguage][getParameterLanguage] );
            });
        }
    });
});
