$(document).ready(function () {
  $('.sidenav').sidenav();
  $('.dropdown-trigger').dropdown();
  $('.materialboxed').materialbox();
  
  $(".wrapper-code").click(function() {
    $( this ).addClass('copied');
  });
  
  $(".wrapper-code").bind({
    click: function() {
      $( this ).addClass('copied');

      var $element = $($(this).find('code'));

      controllCopyPaste($element);
    },
    mouseleave: function() {
      $( this ).removeClass('copied');
    }
  });

  $(".wrapper-code").each(function() {
    var $element = $($(this).find('code'));

    var $temp = $("<textarea>");
    $("body").append($temp);
    $temp.val($element.text())

    $( this ).find('.code').html($temp.val());

    $temp.remove();
  });
});

function controllCopyPaste( $element ) {
  var $temp = $("<textarea>");
  $("body").append($temp);
  $temp.val($element.text()).select();
  document.execCommand("copy");

  return $temp.remove();
}