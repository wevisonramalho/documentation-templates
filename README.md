# Flamboyant Framework


#### Preparação para o desenvolvimento

###### Pré-requisitos :

- Minimo Node.js versão 12;
- GIT;
- Chaves SSH criadas no diretório "pasta de usuario"/.ssh/ e cadastrada no repositório.

###### Dependências de build:

- **Instalando** o `Flamboyant-cli` **global**: Ao utilizar o comando de instalação global é possível utilizar em qualquer projeto o comando `fb comando`.

`npm i -g git+ssh://git@gitlab.com:learning_solutions/flamboyant/flamboyant-cli.git`

- **Instalando** o `Flamboyant-cli` **local**: Ao utilizar o comando de instalação local utilizar sempre o comando npx antes do fb `npx fb comando`. Por default ao baixar as depências a Flamboyant-cli já é baixada localmente no projeto.

`npm i git+ssh://git@gitlab.com:learning_solutions/flamboyant/flamboyant-cli.git`

#### Comandos disponíveis do Flamboyant-cli.

| Comandos  | Resultados  |
| ------------ | ------------ |
| `npx fb i`  | Instala as dependências que estão na flamboyant.json.  |
| `npx fb prod`  | Constrói o projeto (dist e config).  |
| `npx fb dev`  | Abre no navegador padrão para desenvolvimento.  |
| `npx fb config`  | Cria o arquivo `config.json` dentro da dist, este arquivo é responsável pela ordenação e controle das páginas na estrutura.  
| `npx fb createFromTemplate _nomeDoDiretorioAserCriado _idDoTemplateAserClonado`  | Cria dentro de src/pages uma nova página contendo o exemplo do template chamado.  |
| `npx fb scorm`  | Gera o arquivo de manifesto do scorm dentro da dist, o título do curso deve ser cadastrado no arquivo `scorm.js`


#### Baixando as dependências e Inicializando o desenvolvimento do projeto

1. `npm i` - baixando as dependências.
2. `npx fb prod` - vai criar o diretório 'dist' do projeto.
3. `npx fb dev` - vai iniciar o liveReload  para desenvolvimento.

#### Comandos scorm dentro da estrutura

Dentro de todas as páginas é possível chamar o metodo `scorm`, abaixo os comandos possíveis.

| Comandos  | Resultados  |
| ------------ | ------------ |
|  `scorm.set('comando scorm', 'valor');`  | Salvando um valor no scorm.  |
|  `scorm.get('comando scorm');`  | Resgatando um valor do scorm.  |
|   `scorm.setCompleted();`  | Enviando o status de completo para o scorm.  |
|   `scorm.saveObject('id', 'objeto, boleano ou string');`  | Salvando um valor no suspend_data.  |
|  `scorm.loadObject('id');`  | Resgatando um valor no suspend_data.  |

#### Estrutura do projeto

###### Estilos:

1. **less/Initial.less:** Neste estilo estão todas as configurações de estililos, como fontes, cores e tudo que for comum em todo o projeto.
2. **less/page.less:** Neste estilo estão os estilos em comum de todas as páginas, pois este estilo é importado em todas.
3. **less/includes/hud/loader.less:** Este é o estilo do pré-loader da estrutura.
4. **less/includes/helpers:** algumas funções less para uso geral.
5. **less/includes/engine.less:** Estilo da engine.

###### Assets:

Arquivos de mídia em geral devem ser colocados neste diretório.

As imagens que estão dentro do diretório img podem ser importadas para o projeto com o seguinte mixin: `+image(filename, classes, alt)`

###### Templates:

Dentro de templates estão todos os mixins de apoio e de base para a construção da "moldura" e das "páginas".

**templates/main/template.estrutura-principal.pug**: Estrutura da base.
**templates/page/template.page-base.pug**: Estrutura das páginas responsivas.
**templates/page/template.page-base-adaptativo.pug**: Estrutura das páginas adaptativas.

###### Scripts:

1.**js/main.js:** Script da estrutura pai.
2.**scripts:** Script gerais, aqui é possível adicionar um script customizado e importar na estrutura. Mixin para importação:`+scriptScripts(name)`
3.**vendor:** Todas as libs externas devem ser colocadas aqui. Mixin para importação:`+scriptVendor(name)`

###### Metodo global:

Na estrutura foi criado uma propriedade chamada `bridge` para conversar entre pais e filhos, isto nos permite criar um método dentro do main.js e acessá-lo dentro das páginas.

    //main.js
	//Criando o metodo dentro do main.js
    bridge.hiddeViewsFromReaderScreen = function () {
    	var elements = '*';
    
    	$(elements).each(function () {
    		if ($(this).css("opacity") == 0) {
    			$(this).attr("aria-hidden", "true");
    		} else {
    			$(this).attr("aria-hidden", "false");
    		}
    	});
    };

- Invocando o metodo dentro do script da página.



        //pages/pageX/script.js
        bridge.hiddeViewsFromReaderScreen();

###### Páginas:

Caso queira criar um script dentro das páginas, crie um arquivo script.js e no index.pug importe o script com o mixin `+scriptPage("script.js")`

- **page/telaX/index.pug**


    //Arquivo index.pug
    //Importando o script.js dentro da página
    extends ../../templates/page/template.page-base.pug
    
    block styles
    	link(rel="stylesheet", href="style.css")
    
    block body
    	//Conteúdo HTML
    
    block scripts
    	+scriptPage('script.js')

- **page/telaX/script.js**



        //Arquivo: script.js
        //Inície o script dentro do events ready como no exemplo abaixo.
        events.on('ready', function () {
            console.log("******* script loaded!");
        });



###### Importando os componentes da Flamboyant para o projeto (isto é apenas necessário quando os componentes da Flamboyant são utilizados.):

Utilize o seguinte mixin para importar os módulos cadastrados no **flamboyant.json**.

`+linkModule(name)`

###### Utilizando Animações

A biblioteca **animate.css** já está importada no projeto. No seguinte site podem ser encontradas todas as animações disponíveis.

https://animate.style/

###### Utilizando Ícones

A **fontawesome** está importada no projeto.

https://fontawesome.com/


# Responsáveis
* Luiz Coelho - luiz.coelho@afferolab.com.br
* Leandro Soares - leandro.soares@afferolab.com.br
* Bruna Freitas - bruna.escudelario@afferolab.com.br